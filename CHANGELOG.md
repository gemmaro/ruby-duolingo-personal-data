# Changelog

## [Unreleased]

### Others

* Add usage

## [0.2.0] - 2023-06-18

### Added

* Direcotry class (`DuolingoPersonalData::Direcotry`)

### Changed, Removed, Fixed

* Changed to not delegate most Array and Hash methods
* Update RBS

### Others

* Set RuboCop config to disable by default
* Lint with RuboCop

## [0.1.0] - 2023-03-19

Initial release.
