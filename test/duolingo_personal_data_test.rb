require 'test_helper'

class DuolingoPersonalDataTest < Test::Unit::TestCase
  test 'VERSION' do
    assert do
      ::DuolingoPersonalData.const_defined?(:VERSION)
    end
  end

  test 'duolingo personal data directory' do
    directory = ::DuolingoPersonalData::Directory.new('fixtures')
    assert_instance_of ::DuolingoPersonalData::Directory, directory
    assert_instance_of ::DuolingoPersonalData::AuthData, directory.auth_data
    assert_instance_of ::DuolingoPersonalData::AvatarImages, directory.avatar_images
    assert_instance_of ::DuolingoPersonalData::BlastEmails, directory.blast_emails
    assert_instance_of ::DuolingoPersonalData::NotifyData, directory.notify_data
    assert_instance_of ::DuolingoPersonalData::FriendsFollow, directory.friends_follow
    assert_instance_of ::DuolingoPersonalData::Inventory, directory.inventory
    assert_instance_of ::DuolingoPersonalData::Languages, directory.languages
    assert_instance_of ::DuolingoPersonalData::Leaderboards, directory.leaderboards
    assert_instance_of ::DuolingoPersonalData::Profile, directory.profile
    assert_instance_of ::DuolingoPersonalData::Stories, directory.stories
    assert_instance_of ::DuolingoPersonalData::StoryCompletions, directory.story_completions
    assert_instance_of ::DuolingoPersonalData::TeacherPrivacySettings, directory.teacher_privacy_settings
  end

  test 'auth data' do
    data = ::DuolingoPersonalData::AuthData.new('fixtures/auth_data.csv')
    assert_equal 'alice', data.user_account_name
    assert_equal 'alice@example.com', data.email_address
    assert_equal '2023-01-02 03:04:05.678901', data.last_update_timestamp
    assert_equal '2022-01-02 03:04:05.678901', data.last_login_attempt_timestamp
    assert_equal '2021-01-02 03:04:05.678901', data.last_authentication_key_refresh_timestamp
  end

  test 'avatar images' do
    images = ::DuolingoPersonalData::AvatarImages.new('fixtures/avatar_images.csv')
    assert_equal URI('https://simg-ssl.duolingo.com/avatars/012345678/ABCDEFGHIJ/_original'), images.first
    assert_equal 12, images.size
  end

  # TODO: avatar report

  test 'Duolingo blast emails' do
    emails = ::DuolingoPersonalData::BlastEmails.new('fixtures/duolingo-blast-emails.csv')
    assert_equal 'gemmaro.dev@gmail.com', emails.email_address
    assert_equal 'en', emails.ui_language
    assert_equal 'fr', emails.learning_language
    assert_equal true, emails.enabled
    assert_equal true, emails.announcement
    assert_equal Time.new(2018, 1, 2, 3, 4, 5), emails.creation_datetime
    # TODO: last bounce
    # TODO: last sent
    # TODO: last open
    # TODO: last click
    assert_equal Time.new(2023, 1, 2, 3, 4, 5), emails.last_session
    assert_equal false, emails.trial_user
    # TODO: has en certificate
    assert_equal 'JP', emails.country
    assert_equal 'web', emails.client
    assert_equal 100, emails.schools_role
  end

  test 'Duolingo notify data' do
    data = ::DuolingoPersonalData::NotifyData.new('fixtures/duolingo-notify-data.csv')
    assert_equal 'alice@example.com', data.email
    assert_instance_of Hash, data.device_ids
    assert_equal %w[0123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqr 123456789abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrs],
                 data.device_ids['ios']
    # TODO: email bounces
  end

  test 'friends follow' do
    follow = ::DuolingoPersonalData::FriendsFollow.new('fixtures/friends-follow.csv')
    assert_equal 1, follow.num_following
    assert_equal 1, follow.num_followers
    assert_equal 0, follow.num_blocking
    assert_equal 0, follow.num_blockers
    assert_equal Time.new(2023, 1, 2, 3, 4, 5), follow.timestamp_generated
  end

  test 'inventory' do
    inventory = ::DuolingoPersonalData::Inventory.new('fixtures/inventory.csv')
    first_lingot_wager = inventory[0]
    assert_equal 'Lingot wager', first_lingot_wager.item_type
    assert_equal Time.new(2023, 1, 2, 1, 2, 3), first_lingot_wager.purchase_datetime
    assert_equal true, first_lingot_wager.active
    assert_equal 5, first_lingot_wager.price_in_virtual_currency
    assert_equal 5, first_lingot_wager.wager_day
    assert_equal 'In-app Purchase', first_lingot_wager.product
    subscription = inventory[3]
    assert_equal 'Immersive Plus', subscription.payment_processor
    assert_equal Time.new(2020, 1, 2, 1, 2, 3), subscription.expected_expiration
    # TODO: code ID
  end

  test 'languages' do
    languages = ::DuolingoPersonalData::Languages.new('fixtures/languages.csv')
    assert_instance_of ::DuolingoPersonalData::Languages, languages
    es = languages['es']
    assert_equal 'en', es.from_language
    assert_equal 4139, es.points
    assert_equal 0, es.skill_learned
    assert_equal 264, es.total_lessons
    assert_equal 231, es.days_active
    assert_equal Time.new(2020, 1, 2, 1, 2, 3), es.last_active
    assert_equal 3, languages['fr'].prior_proficiency
    assert_equal Time.new(2018, 1, 2, 1, 2, 3), languages['ar'].subscribed
  end

  test 'leaderboard' do
    board = ::DuolingoPersonalData::Leaderboards.new('fixtures/leaderboards.csv')
    assert_equal Time.new(2019, 6, 3, 11, 49, 29), board[0].timestamp
    assert_equal 0, board[0].tier
    assert_equal 272, board[0].score
    assert_equal 3, board.size
  end

  test 'profile' do
    profile = ::DuolingoPersonalData::Profile.new('fixtures/profile.csv')
    assert_equal 'alice', profile.username
    assert_equal 'alice@example.com', profile.email
    assert_equal 'alice', profile.fullname
    # TODO: bio
    assert_equal Time.new(2018, 1, 2, 1, 2, 3), profile.joined_at
    assert_equal 'en', profile.ui_language
    assert_equal 'fr', profile.learning_language
    assert_equal 2615, profile.lingots
    assert_equal 50, profile.daily_goal
    assert_equal 'Asia/Tokyo', profile.timezone
    assert_equal '//simg-ssl.duolingo.com/avatars/012345678/0123456789', profile.avatar_url
    # TODO: previous email addresses
  end

  test 'stories' do
    stories = ::DuolingoPersonalData::Stories.new('fixtures/stories.csv')
    assert_equal '012345678', stories.user_id
    assert_equal Time.new(2018, 1, 2, 1, 2, 3), stories.date_of_first_visit_to_stories
  end

  test 'stories story completions' do
    completions = ::DuolingoPersonalData::StoryCompletions.new('fixtures/stories-story-completions.csv')
    assert_equal '012345678', completions[0].user_id
    assert_equal 'de-zimmer-zu-vermieten', completions[0].story_id
    assert_equal 8, completions[0].score
    assert_equal Time.new(2019, 1, 2, 1, 2, 3), completions[0].time
    # TODO: mode
  end

  test 'teacher privacy settings' do
    settings = ::DuolingoPersonalData::TeacherPrivacySettings.new('fixtures/TeacherPrivacySettings.csv')
    assert_equal false, settings.disable_clubs
    assert_equal true, settings.disable_discussions
    assert_equal true, settings.disable_events
    assert_equal true, settings.disable_stream
    assert_equal false, settings.disable_immersion
    assert_equal true, settings.disable_mature_words
  end
end
