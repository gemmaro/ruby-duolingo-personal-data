# DuolingoPersonalData

## Installation

Install the gem and add to the application's Gemfile by executing:

```shell-session
bundle add duolingo_personal_data
```

If bundler is not being used to manage dependencies, install the gem by executing:

```shell-session
gem install duolingo_personal_data
```

## Usage

```ruby
dir = DuolingoPersonalData::Directory.new('path/to/extracted/directory')
dir.profile.username
dir.leaderboards.first.score
```

## Development

For Guix user, run `guix shell` to setup development environment.
Otherwise, run `bin/setup` to install dependencies.

Then, run `rake test-unit` to run the tests.
You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`.
To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitLab at <https://gitlab.com/gemmaro/ruby-duolingo-personal_data>.

## License

The gem is available as open source under the terms of [The 2.0 version of the Apache License](https://www.apache.org/licenses/LICENSE-2.0).
