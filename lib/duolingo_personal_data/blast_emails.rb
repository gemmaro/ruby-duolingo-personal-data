require 'csv'
require 'time'

module DuolingoPersonalData
  class BlastEmails
    def initialize(csv_path)
      @csv_path = csv_path
    end

    def email_address
      @email_address ||= value_from_property('email_address')
    end

    def ui_language
      @ui_language ||= value_from_property('ui_language')
    end

    def learning_language
      @learning_language ||= value_from_property('learning_language')
    end

    def enabled
      @enabled ||= boolean_value_from_property('enabled')
    end

    def announcement
      @announcement ||= boolean_value_from_property('announcement')
    end

    def creation_datetime
      @creation_datetime ||= time_value_from_property('creation_datetime')
    end

    def last_session
      @last_session ||= time_value_from_property('last_session')
    end

    def trial_user
      @trial_user ||= boolean_value_from_property('trial_user')
    end

    def country
      @country ||= value_from_property('country')
    end

    def client
      @client ||= value_from_property('client')
    end

    def schools_role
      @schools_role ||= integer_value_from_property('schools_role')
    end

    private

    def value_from_property(property_name)
      table.find { |row| row['property'] == property_name }['value']
    end

    def boolean_value_from_property(property_name)
      value = value_from_property(property_name)
      case value
      when '0' then false
      when '1' then true
      else
        raise Error, "cannot interpret #{value.inspect} as boolean"
      end
    end

    def time_value_from_property(property_name)
      value = value_from_property(property_name)
      Time.strptime(value, '%Y-%m-%d %T')
    end

    def integer_value_from_property(property_name)
      value = value_from_property(property_name)
      Integer(value)
    end

    def table
      @table ||= CSV.read(@csv_path, headers: true)
    end
  end
end
