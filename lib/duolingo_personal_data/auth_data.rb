require 'csv'

module DuolingoPersonalData
  class AuthData
    def initialize(csv_path)
      @csv_path = csv_path
    end

    def user_account_name
      @user_account_name ||= value_from_property('User Account Name')
    end

    def email_address
      @email_address ||= value_from_property('Email Address')
    end

    def last_update_timestamp
      @last_update_timestamp ||= value_from_property('Last Update Timestamp')
    end

    def last_login_attempt_timestamp
      @last_login_attempt_timestamp ||= value_from_property('Last Login Attempt Timestamp')
    end

    def last_authentication_key_refresh_timestamp
      @last_authentication_key_refresh_timestamp ||= value_from_property('Last Authentication Key Refresh Timestamp')
    end

    private

    def value_from_property(property_name)
      table.find { |row| row['Property'] == property_name }['Value']
    end

    def table
      @table ||= CSV.read(@csv_path, headers: true)
    end
  end
end
