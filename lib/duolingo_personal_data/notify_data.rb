require 'json'

module DuolingoPersonalData
  class NotifyData
    def initialize(csv_path)
      @csv_path = csv_path
    end

    def email
      @email ||= value_from_property('email')
    end

    def device_ids
      @device_ids ||= json_value_from_property('device_ids')
    end

    private

    def value_from_property(property_name)
      table.find { |row| row['property'] == property_name }['value']
    end

    def json_value_from_property(property_name)
      value = value_from_property(property_name)
      value.gsub!("'", '"')
      JSON.parse(value)
    end

    def table
      @table ||= CSV.read(@csv_path, headers: true)
    end
  end
end
