require 'forwardable'

module DuolingoPersonalData
  InventoryItem = Struct.new(:item_type, :purchase_datetime, :active, :price_in_virtual_currency, :wager_day,
                             :payment_processor, :product, :expected_expiration, keyword_init: true)

  class Inventory
    def initialize(csv_path)
      @csv_path = csv_path
    end

    extend Forwardable
    def_delegators :items, :to_a, :first, :size, :[] # TODO: Add more as needed

    private

    def items
      @items ||= table.map do |row|
        item = InventoryItem.new(item_type: row['item_type'],
                                 purchase_datetime: parse_datetime(row['purchase_datetime']), active: parse_boolean(row['active']), payment_processor: row['payment_processor'], product: row['product'])
        price = row['price_in_virtual_currency']
        item.price_in_virtual_currency = Integer(price) if price
        day = row['wager_day']
        item.wager_day = Integer(day) if day
        expiration = row['expected_expiration']
        item.expected_expiration = parse_datetime(expiration) if expiration
        item
      end
    end

    def parse_datetime(str)
      Time.strptime(str, '%Y-%m-%d %T')
    end

    def parse_boolean(str)
      case str
      when 'false' then false
      when 'true' then true
      else
        raise Error, "cannot parse #{str.inspect} as boolean"
      end
    end

    def table
      @table ||= CSV.read(@csv_path, headers: true)
    end
  end
end
