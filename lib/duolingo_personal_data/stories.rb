module DuolingoPersonalData
  class Stories
    def initialize(csv_path)
      @csv_path = csv_path
    end

    def user_id
      @user_id ||= value_from_property('userId')
    end

    def date_of_first_visit_to_stories
      @date_of_first_visit_to_stories ||= datetime_value_from_property('dateOfFirstVisitToStories')
    end

    private

    def value_from_property(property_name)
      table.first[property_name]
    end

    def datetime_value_from_property(property_name)
      Time.strptime(value_from_property(property_name), '%F %T')
    end

    def table
      @table ||= CSV.read(@csv_path, headers: true)
    end
  end
end
