module DuolingoPersonalData
  class TeacherPrivacySettings
    def initialize(csv_path)
      @csv_path = csv_path
    end

    def disable_clubs
      @disable_clubs ||= boolean_value_from_property('disable_clubs')
    end

    def disable_discussions
      @disable_discussions ||= boolean_value_from_property('disable_discussions')
    end

    def disable_events
      @disable_events ||= boolean_value_from_property('disable_events')
    end

    def disable_stream
      @disable_stream ||= boolean_value_from_property('disable_stream')
    end

    def disable_immersion
      @disable_immersion ||= boolean_value_from_property('disable_immersion')
    end

    def disable_mature_words
      @disable_mature_words ||= boolean_value_from_property('disable_mature_words')
    end

    private

    def boolean_value_from_property(property_name)
      value = value_from_property(property_name)
      case value
      when 'False' then false
      when 'True' then true
      else
        raise Error, "cannot interpret #{value.inspect} as boolean"
      end
    end

    def value_from_property(property_name)
      table.first[property_name]
    end

    def table
      @table ||= CSV.read(@csv_path, headers: true)
    end
  end
end
