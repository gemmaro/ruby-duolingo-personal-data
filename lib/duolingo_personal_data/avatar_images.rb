require 'csv'
require 'uri'
require 'forwardable'

module DuolingoPersonalData
  class AvatarImages
    def initialize(csv_path)
      @csv_path = csv_path
    end

    extend Forwardable
    def_delegators :urls, :to_a, :first, :size # TODO: Add more as needed

    private

    def urls
      @urls ||= table.map { |row| URI(row['URLs']) }
    end

    def table
      @table ||= CSV.read(@csv_path, headers: true)
    end
  end
end
