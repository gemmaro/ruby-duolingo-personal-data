require 'forwardable'
require 'csv'

module DuolingoPersonalData
  Language = Struct.new(:from_language, :points, :skill_learned, :total_lessons, :days_active, :last_active,
                        :prior_proficiency, :subscribed, keyword_init: true)
  class Languages
    def initialize(csv_path)
      @csv_path = csv_path
    end

    extend Forwardable
    def_delegators :languages, :to_h, :[] # TODO: Add more as needed

    private

    def languages
      @languages ||= CSV.read(@csv_path, headers: true).map do |row|
        language = Language.new(from_language: row['from_language'])
        points = row['points']
        language.points = Integer(points) if points
        skills = row['skills_learned']
        language.skill_learned = Integer(skills) if skills
        lessons = row['total_lessons']
        language.total_lessons = Integer(lessons) if lessons
        days = row['days_active']
        language.days_active = Integer(days) if days
        active = row['last_active']
        language.last_active = parse_datetime(active) if active
        proficiency = row['prior_proficiency']
        language.prior_proficiency = Integer(proficiency) if proficiency
        subscribed = row['subscribed']
        language.subscribed = parse_datetime(subscribed) if subscribed
        { row['learning_language'] => language }
      end.reduce(&:merge)
    end

    def parse_datetime(str)
      Time.strptime(str, '%Y-%m-%d %T')
    end
  end
end
