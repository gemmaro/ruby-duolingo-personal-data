module DuolingoPersonalData
  class Directory
    def initialize(path)
      @path = path
    end

    def auth_data
      @auth_data ||= AuthData.new(File.join(@path, 'auth_data.csv'))
    end

    def avatar_images
      @avatar_images ||= AvatarImages.new(File.join(@path, 'avatar_images.csv'))
    end

    def blast_emails
      @blast_emails ||= BlastEmails.new(File.join(@path, 'duolingo-blast-emails.csv'))
    end

    def notify_data
      @notify_data ||= NotifyData.new(File.join(@path, 'duolingo-notify-data.csv'))
    end

    def friends_follow
      @friends_follow ||= FriendsFollow.new(File.join(@path, 'friends-follow.csv'))
    end

    def inventory
      @inventory ||= Inventory.new(File.join(@path, 'inventory.csv'))
    end

    def languages
      @languages ||= Languages.new(File.join(@path, 'languages.csv'))
    end

    def leaderboards
      @leaderboards ||= Leaderboards.new(File.join(@path, 'leaderboards.csv'))
    end

    def profile
      @profile ||= Profile.new(File.join(@path, 'profile.csv'))
    end

    def stories
      @stories ||= Stories.new(File.join(@path, 'stories.csv'))
    end

    def story_completions
      @story_completions ||= StoryCompletions.new(File.join(@path, 'stories-story-completions.csv'))
    end

    def teacher_privacy_settings
      @teacher_privacy_settings ||= TeacherPrivacySettings.new(File.join(@path, 'TeacherPrivacySettings.csv'))
    end
  end
end
