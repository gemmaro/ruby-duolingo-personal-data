module DuolingoPersonalData
  class FriendsFollow
    def initialize(csv_path)
      @csv_path = csv_path
    end

    def num_following
      @num_following ||= integer_value_from_property('num_following')
    end

    def num_followers
      @num_followers ||= integer_value_from_property('num_followers')
    end

    def num_blocking
      @num_blocking ||= integer_value_from_property('num_blocking')
    end

    def num_blockers
      @num_blockers ||= integer_value_from_property('num_blockers')
    end

    def timestamp_generated
      @timestamp_generated ||= timestamp_value_from_property('timestamp_generated')
    end

    private

    def value_from_property(property_name)
      table.first[property_name]
    end

    def integer_value_from_property(property_name)
      value = value_from_property(property_name)
      Integer(value)
    end

    def timestamp_value_from_property(property_name)
      value = integer_value_from_property(property_name)
      Time.at(value)
    end

    def table
      @table ||= CSV.read(@csv_path, headers: true)
    end
  end
end
