require 'csv'
require 'forwardable'

module DuolingoPersonalData
  LeaderboardsEntry = Struct.new(:timestamp, :tier, :score, keyword_init: true)

  class Leaderboards
    def initialize(csv_path)
      @csv_path = csv_path
    end

    extend Forwardable
    def_delegators :leaderboards_entries, :to_a, :first, :size, :[] # TODO: Add more as needed

    private

    def leaderboards_entries
      @leaderboards_entries ||= CSV.read(@csv_path, headers: true).map do |row|
        LeaderboardsEntry.new(timestamp: Time.strptime(row['timestamp'], '%FT%TZ'), tier: Integer(row['tier']),
                              score: Float(row['score']))
      end
    end
  end
end
