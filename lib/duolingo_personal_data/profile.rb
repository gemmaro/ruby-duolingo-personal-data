require 'csv'

module DuolingoPersonalData
  class Profile
    def initialize(csv_path)
      @csv_path = csv_path
    end

    def username
      @username ||= value_from_property('username')
    end

    def email
      @email ||= value_from_property('email')
    end

    def fullname
      @fullname ||= value_from_property('fullname')
    end

    def joined_at
      @joined_at ||= datetime_value_from_property('joined_at')
    end

    def ui_language
      @ui_language ||= value_from_property('ui_language')
    end

    def learning_language
      @learning_language ||= value_from_property('learning_language')
    end

    def lingots
      @lingots ||= integer_value_from_property('lingots')
    end

    def daily_goal
      @daily_goal ||= integer_value_from_property('daily_goal')
    end

    def timezone
      @timezone ||= value_from_property('timezone')
    end

    def avatar_url
      @avatar_url ||= value_from_property('avatar_url')
    end

    private

    def value_from_property(property_name)
      table.find { |row| row['name'] == property_name }['value']
    end

    def integer_value_from_property(property_name)
      Integer(value_from_property(property_name))
    end

    def datetime_value_from_property(property_name)
      Time.strptime(value_from_property(property_name), '%F %T')
    end

    def table
      @table ||= CSV.read(@csv_path, headers: true)
    end
  end
end
