require 'forwardable'

module DuolingoPersonalData
  StoryCompletion = Struct.new(:user_id, :story_id, :score, :time, keyword_init: true)

  class StoryCompletions
    def initialize(csv_path)
      @csv_path = csv_path
    end

    extend Forwardable
    def_delegators :completions, :[] # TODO: Add more as needed

    private

    def completions
      @completions ||= CSV.read(@csv_path, headers: true).map do |row|
        StoryCompletion.new(user_id: row['userId'], story_id: row['storyId'], score: Integer(row['score']),
                            time: Time.strptime(row['time'], '%F %T'))
      end
    end
  end
end
