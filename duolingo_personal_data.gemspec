require_relative 'lib/duolingo_personal_data/version'

Gem::Specification.new do |spec|
  spec.name = 'duolingo_personal_data'
  spec.version = DuolingoPersonalData::VERSION
  spec.authors = ['gemmaro']
  spec.email = ['gemmaro.dev@gmail.com']

  spec.summary = 'Library for Duolingo personal data'
  spec.description = 'Duolingo Personal Data gem is for loading Duolingo Personal Data, which can be acquired at https://drive-thru.duolingo.com/.'
  spec.homepage = 'https://gitlab.com/gemmaro/ruby-duolingo-personal-data'
  spec.license = 'Apache-2.0'
  spec.required_ruby_version = '>= 2.6.0'

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = spec.homepage
  spec.metadata['changelog_uri'] = "#{spec.homepage}/-/blob/main/CHANGELOG.md"

  spec.files = Dir.chdir(__dir__) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:bin|test|spec|features)/|\.(?:git|circleci)|appveyor)})
    end
  end

  spec.require_paths = ['lib']
  spec.metadata['rubygems_mfa_required'] = 'true'
end
