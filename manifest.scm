(use-modules (gnu packages ruby))

(packages->manifest (list ruby-rubocop ruby-rubocop-rake ruby bundler))
